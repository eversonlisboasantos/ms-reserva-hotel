# Reserva Hotel API

APi desenvolvida com o objetivo de encontrar a menor taxa entre os 3 hoteis, Lakewood, Bridgewood e Ridgewood.

#### Tecnologias
  - Java 8
  - Spring boot
  - AWS Cloud
#### Repositorio
https://bitbucket.org/eversonlisboasantos/ms-reserva-hotel/src/master/

#### Collection
https://www.getpostman.com/collections/b83e62099e344d186949

#### Solução do problema
Recuperar as taxas de todos os hoteis dos dias informados, fazer a somatoria e tirar a media,

#### Endpoints
http://reserva-hotel-api-dev.us-west-2.elasticbeanstalk.com/hoteis

Body Request : {
    "tipoCliente": "REWARD",
    "datas": [
        "09Aug2020"
    ]
}

http://reserva-hotel-api-dev.us-west-2.elasticbeanstalk.com/hoteis
Body Request: arquivo com a extensão .txt, com o layout abaixo:

Regular: 16Mar2009, 17Mar2009, 18Mar2009
