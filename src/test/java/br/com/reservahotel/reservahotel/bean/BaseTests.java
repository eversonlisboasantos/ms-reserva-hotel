package br.com.reservahotel.reservahotel.bean;

import br.com.reservahotel.reservahotel.dto.ResquestReservaDto;
import br.com.reservahotel.reservahotel.service.ReservaService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
@SpringBootTest
public class BaseTests {

    @Autowired
    public ReservaService reservaService;

    protected Hotel lakewood = null;
    protected Hotel bridgewood = null;
    protected Hotel ridgeWood = null;
    protected ResquestReservaDto r1 = null;
    protected List<Taxa> taxasTodosHoteis = null;

    @BeforeEach
    void mockHotelTests() {
        taxasTodosHoteis = new ArrayList<>();

        taxasTodosHoteis.add(new Taxa(110, true, true, lakewood));
        taxasTodosHoteis.add(new Taxa(80, false, true, lakewood));
        taxasTodosHoteis.add(new Taxa(90, true, false, lakewood));
        taxasTodosHoteis.add(new Taxa(80, false, false, lakewood));

        taxasTodosHoteis.add(new Taxa(160, true, true, bridgewood));
        taxasTodosHoteis.add(new Taxa(110, false, true, bridgewood));
        taxasTodosHoteis.add(new Taxa(60, true, false, bridgewood));
        taxasTodosHoteis.add(new Taxa(50, false, false, bridgewood));

        taxasTodosHoteis.add(new Taxa(220, true, true, ridgeWood));
        taxasTodosHoteis.add(new Taxa(100, false, true, ridgeWood));
        taxasTodosHoteis.add(new Taxa(150, true, false, ridgeWood));
        taxasTodosHoteis.add(new Taxa(40, false, false, ridgeWood));
    }
}
