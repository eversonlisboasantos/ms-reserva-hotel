package br.com.reservahotel.reservahotel.service;

import br.com.reservahotel.reservahotel.bean.BaseTests;
import br.com.reservahotel.reservahotel.dominio.TipoCliente;
import br.com.reservahotel.reservahotel.dto.ResponseReservaHotelDTO;
import br.com.reservahotel.reservahotel.dto.ResquestReservaDto;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReservaServiceTests extends BaseTests {


    @Test
    public void calculaDiariaMaisBarataClienteRegurlarDiaSemana() {

        r1 = new ResquestReservaDto(TipoCliente.REGULAR);
        LocalDate date = LocalDate.of(2020,10, 9);
        r1.getDatas().add(date);

        ResponseReservaHotelDTO responseReservaHotelDTO = this.reservaService.buscaHotelMaisBaratoPorDataTipoCliente(r1);
        assertEquals("lakewood", responseReservaHotelDTO.getNomeHotel());
    }

    @Test
    public void calculaDiariaMaisBarataClienteRegurlarFimDeSemana() {

        r1 = new ResquestReservaDto(TipoCliente.REGULAR);
        LocalDate date = LocalDate.of(2020,10, 11);
        r1.getDatas().add(date);

        ResponseReservaHotelDTO responseReservaHotelDTO = this.reservaService.buscaHotelMaisBaratoPorDataTipoCliente(r1);
        assertEquals("bridgewood", responseReservaHotelDTO.getNomeHotel());
    }

    @Test
    public void calculaDiariaMaisBarataClienteRewardFimDeSemana() {

        r1 = new ResquestReservaDto(TipoCliente.REWARD);
        LocalDate date = LocalDate.of(2020,10, 10);
        r1.getDatas().add(date);

        ResponseReservaHotelDTO responseReservaHotelDTO = this.reservaService.buscaHotelMaisBaratoPorDataTipoCliente(r1);
        assertEquals("ridgeWood", responseReservaHotelDTO.getNomeHotel());
    }

    @Test
    public void calculaDiariaMaisBarataClienteRewardDiaSemana() {

        r1 = new ResquestReservaDto(TipoCliente.REWARD);
        LocalDate date = LocalDate.of(2020,10, 9);
        r1.getDatas().add(date);
        ResponseReservaHotelDTO responseReservaHotelDTO = this.reservaService.buscaHotelMaisBaratoPorDataTipoCliente(r1);
        assertEquals("lakewood", responseReservaHotelDTO.getNomeHotel());
    }

    @Test
    public void isSemana() {
        LocalDate date = LocalDate.of(2020,10, 9);
        int diaSemana = date.getDayOfWeek().getValue();
        assertTrue(diaSemana < 6 );
    }

}
