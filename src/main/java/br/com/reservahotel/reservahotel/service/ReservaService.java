package br.com.reservahotel.reservahotel.service;

import br.com.reservahotel.reservahotel.bean.Hotel;
import br.com.reservahotel.reservahotel.bean.Taxa;
import br.com.reservahotel.reservahotel.dominio.TipoCliente;
import br.com.reservahotel.reservahotel.dto.ResponseReservaHotelDTO;
import br.com.reservahotel.reservahotel.dto.ResquestReservaDto;
import br.com.reservahotel.reservahotel.exception.ValidacaoArquivoException;
import com.fasterxml.jackson.databind.exc.ValueInstantiationException;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ReservaService {

    private static final Hotel lakewood = new Hotel("lakewood", 3);
    private static final Hotel bridgewood = new Hotel("bridgewood", 4);
    private static final Hotel ridgeWood = new Hotel("ridgeWood", 5);
    private static final List<Taxa> taxasTodosHoteis = new ArrayList<>();

    static {

        taxasTodosHoteis.add(new Taxa(110, true, true, lakewood));
        taxasTodosHoteis.add(new Taxa(80, false, true, lakewood));
        taxasTodosHoteis.add(new Taxa(90, true, false, lakewood));
        taxasTodosHoteis.add(new Taxa(80, false, false, lakewood));

        taxasTodosHoteis.add(new Taxa(160, true, true, bridgewood));
        taxasTodosHoteis.add(new Taxa(110, false, true, bridgewood));
        taxasTodosHoteis.add(new Taxa(60, true, false, bridgewood));
        taxasTodosHoteis.add(new Taxa(50, false, false, bridgewood));

        taxasTodosHoteis.add(new Taxa(220, true, true, ridgeWood));
        taxasTodosHoteis.add(new Taxa(100, false, true, ridgeWood));
        taxasTodosHoteis.add(new Taxa(150, true, false, ridgeWood));
        taxasTodosHoteis.add(new Taxa(40, false, false, ridgeWood));
    }

    public ResponseReservaHotelDTO buscaHotelMaisBaratoPorDataTipoCliente(ResquestReservaDto request) {
        boolean regular = request.getTipoCliente() == TipoCliente.REGULAR ? true : false;

        List<Taxa> taxasFiltradasPorDatas = new ArrayList<>();
        this.filtraTaxasPorDias(taxasFiltradasPorDatas, request.getDatas());

        Stream<Taxa> taxasPorTipoDeCliente = this.filtraTaxasPorTipoDeCliente(taxasFiltradasPorDatas, regular);
        Map<Hotel, DoubleSummaryStatistics> collect = this.agrupaValorTaxasPorHotel(taxasPorTipoDeCliente);
        Hotel hotel = this.calculaMenorTaxa(collect);
        return new ResponseReservaHotelDTO(hotel.getNome());
    }

    public ResponseReservaHotelDTO buscaHotelMaisBaratoPorDataTipoCliente(MultipartFile arquivo) throws IOException, ValidacaoArquivoException {
        if(arquivo.isEmpty())
            throw new ValidacaoArquivoException("Arquivo vazio", "O arquivo não possui informações para serem processadas");

        if(!arquivo.getOriginalFilename().endsWith(".txt"))
            throw new ValidacaoArquivoException("Extensão invalida", "A entensão do arquivo precisa ser .txt");

        ResponseReservaHotelDTO hotelDTO = this.buscaHotelMaisBaratoPorDataTipoCliente(this.lerArquivo(arquivo));
        return new ResponseReservaHotelDTO(hotelDTO.getNomeHotel());
    }

    private void filtraTaxasPorDias(List<Taxa> taxasFiltradasPorDatas, List<LocalDate> datas) {
        datas.stream().forEach(date -> {
            taxasFiltradasPorDatas.addAll(taxasTodosHoteis.stream()
                    .filter(taxa -> taxa.isSemana() == isSemana(date))
                    .collect(Collectors.toList()));
        });
    }

    private Stream<Taxa> filtraTaxasPorTipoDeCliente(List<Taxa> taxasFiltradasPorDatas, boolean regular) {
        return taxasFiltradasPorDatas.stream().filter(t -> t.isRegular() == regular);
    }

    private Map<Hotel, DoubleSummaryStatistics> agrupaValorTaxasPorHotel(Stream<Taxa> taxasPorTipoDeCliente) {
        return taxasPorTipoDeCliente.collect(Collectors.groupingBy(taxa ->
                taxa.getHotel(), Collectors.summarizingDouble(Taxa::getVlDiaria)));
    }

    private Hotel calculaMenorTaxa(Map<Hotel, DoubleSummaryStatistics> collect) {
        return collect.entrySet().stream().reduce((v1, v2) -> {
            if (v1.getValue().getAverage() == v2.getValue().getAverage()) {
                if (v1.getKey().getClassificacao() < v2.getKey().getClassificacao()) {
                    return v2;
                }
                return v1;
            }
            if (v1.getValue().getAverage() < v2.getValue().getAverage())
                return v1;
            return v2;
        }).get().getKey();
    }

    public boolean isSemana(LocalDate data) {
        return data.getDayOfWeek().getValue() < 6;
    }

    private ResquestReservaDto lerArquivo(MultipartFile arquivo) throws IOException, ValidacaoArquivoException {

        File file = new File("entrada.txt");
        try (OutputStream os = new FileOutputStream(file)) {
            os.write(arquivo.getBytes());
        } catch (IOException io) {
            throw new ValidacaoArquivoException(" Erro na leitura do arquivo","Não foi possivel ler o arquivo informado, verifique as informações ou o layout do arquivo");
        }

        DateTimeFormatter dMMMyyyy = new DateTimeFormatterBuilder().parseCaseInsensitive()
                .appendPattern("dMMMyyyy")
                .toFormatter(Locale.ENGLISH);

        List<String> lines = Files.readAllLines(Paths.get(file.getPath()));
        ResquestReservaDto resquestReservaDtoStream = lines.stream().map(s -> {
            String[] strings = s.split(":");
            ResquestReservaDto dto = new ResquestReservaDto(TipoCliente.valueOf(strings[0].trim().toUpperCase()));
            List<String> datas = Arrays.stream(strings[1].split(",")).collect(Collectors.toList());
            datas.stream().forEach(s1 -> {
                dto.getDatas().add(LocalDate.parse(s1.trim(), dMMMyyyy));
            });
            return dto;
        }).findFirst().get();
        return resquestReservaDtoStream;
    }
}
