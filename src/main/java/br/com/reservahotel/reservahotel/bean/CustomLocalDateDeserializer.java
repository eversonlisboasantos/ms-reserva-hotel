package br.com.reservahotel.reservahotel.bean;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

@JsonComponent
public class CustomLocalDateDeserializer extends JsonDeserializer<LocalDate> {


    public CustomLocalDateDeserializer() {
    }

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return LocalDate.parse(jsonParser.readValueAs(String.class), new DateTimeFormatterBuilder().parseCaseInsensitive()
                .appendPattern("dMMMyyyy")
                .toFormatter(Locale.ENGLISH));
    }

}
