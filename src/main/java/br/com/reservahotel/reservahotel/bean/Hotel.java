package br.com.reservahotel.reservahotel.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class Hotel {

    private String nome;
    private int classificacao;

    public Hotel(String nome, int classificacao) {
        this.nome = nome;
        this.classificacao = classificacao;
    }
}
