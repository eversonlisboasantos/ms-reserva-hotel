package br.com.reservahotel.reservahotel.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Taxa {
    private double vlDiaria;
    private boolean regular;
    private boolean semana;
    private Hotel hotel;

    public Taxa(double vlDiaria, boolean regular, boolean semana, Hotel hotel) {
        this.vlDiaria = vlDiaria;
        this.regular = regular;
        this.semana = semana;
        this.hotel = hotel;
    }
}
