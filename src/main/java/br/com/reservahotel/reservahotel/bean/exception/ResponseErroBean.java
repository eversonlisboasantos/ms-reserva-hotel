package br.com.reservahotel.reservahotel.bean.exception;

import lombok.Data;

@Data
public class ResponseErroBean {

    private String message;
    private String detalhes;

    public ResponseErroBean() {
    }

    public ResponseErroBean(String message, String detalhes) {
        this.message = message;
        this.detalhes = detalhes;
    }
}
