package br.com.reservahotel.reservahotel.bean.exception;

import br.com.reservahotel.reservahotel.exception.ValidacaoArquivoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ValidacaoArquivoException.class)
    public ResponseEntity<ResponseErroBean> handleBusinessException(ValidacaoArquivoException e){
        ResponseErroBean responseErroBean = new ResponseErroBean(e.getMessage(), e.getDetalhes());
        return new ResponseEntity<>(responseErroBean, HttpStatus.BAD_REQUEST);
    }
}
