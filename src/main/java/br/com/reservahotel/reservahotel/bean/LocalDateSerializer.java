package br.com.reservahotel.reservahotel.bean;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
@JsonComponent
public class LocalDateSerializer extends StdSerializer<LocalDate> {

    private SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyy");

    public LocalDateSerializer(){
        super(LocalDate.class);
    }

    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(format.format(localDate));
    }
}
