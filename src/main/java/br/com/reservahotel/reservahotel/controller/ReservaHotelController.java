package br.com.reservahotel.reservahotel.controller;

import br.com.reservahotel.reservahotel.dto.ResponseReservaHotelDTO;
import br.com.reservahotel.reservahotel.dto.ResquestReservaDto;
import br.com.reservahotel.reservahotel.exception.ValidacaoArquivoException;
import br.com.reservahotel.reservahotel.service.ReservaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/hoteis")
public class ReservaHotelController {

    @Autowired
    private ReservaService reservaService;

    @GetMapping
    public ResponseReservaHotelDTO buscaHotel(@RequestBody  ResquestReservaDto request){
        return reservaService.buscaHotelMaisBaratoPorDataTipoCliente(request);
    }

    @PostMapping
    public ResponseReservaHotelDTO buscaHotel (@RequestParam("arquivo") MultipartFile file) throws IOException, ValidacaoArquivoException {
        return reservaService.buscaHotelMaisBaratoPorDataTipoCliente(file);
    }
}
