package br.com.reservahotel.reservahotel.exception;

import lombok.Data;

@Data
public class ValidacaoArquivoException extends RuntimeException {

    private String mensagem;
    private String detalhes;

    public ValidacaoArquivoException(){}

    public ValidacaoArquivoException(String mensagem, String detalhes) {
        super(mensagem);
        this.mensagem = mensagem;
        this.detalhes = detalhes;
    }

    public ValidacaoArquivoException(String message, Throwable cause, String mensagem, String detalhes) {
        super(message, cause);
        this.mensagem = mensagem;
        this.detalhes = detalhes;
    }
}
