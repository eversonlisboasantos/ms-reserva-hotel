package br.com.reservahotel.reservahotel.dominio;

public enum TipoCliente {

    REGULAR ("REGULAR"),
    REWARD ("REWARD");

    private String value;

    TipoCliente(String value) {
        this.value = value;
    }
}
