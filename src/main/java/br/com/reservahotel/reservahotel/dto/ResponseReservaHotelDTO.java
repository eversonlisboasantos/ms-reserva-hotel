package br.com.reservahotel.reservahotel.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseReservaHotelDTO implements Serializable {

    private String nomeHotel;

    public ResponseReservaHotelDTO(String nomeHotel) {
        this.nomeHotel = nomeHotel;
    }
}
