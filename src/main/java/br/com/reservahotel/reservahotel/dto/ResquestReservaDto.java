package br.com.reservahotel.reservahotel.dto;

import br.com.reservahotel.reservahotel.bean.CustomLocalDateDeserializer;
import br.com.reservahotel.reservahotel.dominio.TipoCliente;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class ResquestReservaDto{

    private TipoCliente tipoCliente;
    @JsonDeserialize(using = CustomLocalDateDeserializer.class)
    @JsonProperty("datas")
    private List<LocalDate> datas;


    public ResquestReservaDto(TipoCliente tipoCliente) {
        this.tipoCliente = tipoCliente;
        this.datas = new ArrayList<>();
    }

    public ResquestReservaDto(TipoCliente tipoCliente, List<LocalDate> datas) {
        this.tipoCliente = tipoCliente;
        this.datas = datas;
    }

    public TipoCliente getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoCliente tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public List<LocalDate> getDatas() {
        return datas;
    }

    public void setDatas(List<LocalDate> datas) {
        this.datas = datas;
    }
}
